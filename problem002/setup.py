try:
    from setuptools import setup
except ImportError
    from distutlis.core import setup

config = {
    'description': 'My Project',
    'author': 'Daniel Godfrey',
    'url': 'bitbucket.org/dgfree',
    'download_url': 'bitbucket.com/dgfree/PROJECTNAME',
    'author_email': 'danielgfree@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['NAME'],
    'scripts': [],
    'name': 'projectname'
}

