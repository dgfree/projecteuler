"""By considering the terms in the Fibonacci sequence whose values do not
exceed four million, find the sum of the even-valued terms.

Results for 5000 iterations (lowest time ever recorded):
lazy  : .062 secs
recur : .891 secs
closed: .070 secs
"""
import math

def even_fib_lazy(n):
    """Lazy way abusing Python's multiple assignment"""
    fibn, fib_plus1 = 2, 8
    for _ in xrange(n):
        fibn, fib_plus1 = fib_plus1, fibn + 4*fib_plus1
    return fibn

def sum_even_fibs_lazy(upper_limit):
    """Sums the even Fibonacci numbers using the 'lazy' method."""
    fib_sum = 0
    n = 0
    highest_sum = 0
    while highest_sum < upper_limit:
        highest_sum = even_fib_lazy(n)
        fib_sum += highest_sum
        n += 1
    return fib_sum

def sum_even_fibs_recur(upper_limit):
    """Sums the even Fibonacci numbers using recursion."""
    fib_sum = 0
    n = 0
    highest_sum = 0
    while highest_sum < upper_limit:
        highest_sum = even_fib_recur(n)
        fib_sum += highest_sum
        n += 1
    return fib_sum

def even_fib_recur(n):
    """Finds the nth Fibonacci number using recursion.
    Recursion formula is:
    F(n) = F(n-1) + 4*F(n-2)
    """
    if n == 0:
        return 2
    elif n == 1:
        return 8
    elif n > 1:
        return 4*even_fib_recur(n-1) + even_fib_recur(n-2)
    else:
        raise Exception('Negative number in even_fib_recur')

def sum_even_fibs_closed(upper_limit):
	"""Sums the even Fibonacci numbers using a closed formula."""
	# constants similar to golden ratio
	phi = 2.0 + math.sqrt(5)
	phi0 =  2.0 - math.sqrt(5)

	# coefficients determined mathematically with generating functions:
	A = (4.0 + 2 * math.sqrt(5)) / (2 * math.sqrt(5))
	B = (-4.0 + 2 * math.sqrt(5)) / (2 * math.sqrt(5))

	iters = number_of_iters(upper_limit, phi)

	fib_sum = 0
	for i in xrange(0, iters + 1):
		fib_sum += ith_even_fib(i, A, B, phi, phi0)

	return int(fib_sum)

def number_of_iters(upper_limit, phi):
    """Finds the number of iterations needed before you reach upper limit"""
    iters = int(math.log10(upper_limit) / math.log10(phi))
    return iters

def ith_even_fib(i, A, B, phi, phi0):
    """Finds the ith even Fibonacci number, using a closed form solution
    This solution was hand calculated.
    """
    even_fib = A * math.pow(phi, i) + B * math.pow(phi0, i)
    return even_fib

def main_lazy():
    sum_even_fibs_lazy(4000000)

def main_recur():
    sum_even_fibs_recur(4000000)

def main_closed():
    sum_even_fibs_closed(4000000)

if __name__ == "__main__":
    import timeit
    print timeit.repeat("main_lazy()",
                        setup="from problem002 import main_lazy",
                        number=5000)
    print timeit.repeat("main_recur()",
                        setup="from problem002 import main_recur",
                        number=5000)
    print timeit.repeat("main_closed()",
                        setup="from problem002 import main_closed",
                        number=5000)
