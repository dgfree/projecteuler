# By starting at the top of the triangle below and moving to adjacent 
# number on the row below, the maximum total from top to bottom is 23.
# 3
# 7 4
# 2 4 6
# 8 5 9 3
# That is, 3 + 7 + 4 + 9 = 23.
# Find the maximum total from top to bottom of the triangle in file.


# Triangle analog will be list of lists, rows by first index
# and columns by the second index.

# Addition will happen with binary strings. The number of paths are equal to 
# the number of binary strings with length equal to the height of the triangle
# minus one. So, I create a bijection of paths to binary strings by denoting a
# move left in a path with a 0 and a move right in a path with a 1. 

def construct_tri(filename):
    """Open the file and construct the triangle. Return tri"""
    tri = []       
    
    try:        
        file = open(filename, 'r')
    
    except IOError:
        raise IOError("The file did not exist")     
    
    # convert the file into list of numbers.
    for line in file:            
        tri.append(map(int, line.split()))
    
    file.close()
    return tri

def nav_tri_left(tri, row, col):
    """Returns the number from a left move"""
    if row > len(tri):
        raise Exception("Column error in left navigation.")

    elif col > row:
        raise Exception("Row error in left navigation.")

    return tri[row + 1][col]

def nav_tri_right(tri, row, col):
    """Returns the number from a right move"""
    if row >= len(tri):
        raise Exception("Column error in right navigation.")

    elif col > row:
        raise Exception("Row error in right navigation.")     
    
    return tri[row + 1][col + 1]

def sum_up_path(tri, bin_str):
    """Takes binary string, creates a path with it, and sums over that path"""   
    # start sum with top of triangle for convenience
    row, col, sum = 0, 0, tri[0][0]
    for i in xrange(0, len(bin_str)):
        
        if bin_str[i] == '0':
            sum += nav_tri_left(tri, row, col)
            row += 1
            
        elif bin_str[i] == '1':
            sum += nav_tri_right(tri, row, col)
            row += 1
            col += 1

    return sum

def find_max(tri):
    """creates a binary number to travel thru all paths and find max"""
    # initialize binary number. Length should be height of tri - 1.
    bin_str = '0'.zfill(len(tri) - 1)
    max = 0

    for i in xrange(0, 2**(len(tri) - 1)):
        sum_of_path = sum_up_path(tri, bin_str)
        
        if max < sum_of_path:
            max = sum_of_path

        # add one to binary while keeping the length
        # bin returns "0b000", etc. so get rid of 0b
        bin_str = bin(int(bin_str, 2) + 1)[2:].zfill(len(tri) - 1)
        
    return max

def main():
    tri = construct_tri("docs/pe18.txt")
    print "This program finds a maximal path along a triangle."
    print "The triangle can be found in docs/pe18.txt."
    print "The maximal path sum is:" 
    print find_max(tri)

if __name__ == "__main__":
    main()
