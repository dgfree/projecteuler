# pe18test.py
# unit test for pe18.py
from Problem018 import Problem018
from nose.tools import *
import unittest

class TestTriangle(unittest.TestCase):
    """Tests using triangle in example for Problem 18."""    
    known_tri = [[3], [7, 4], [2, 4, 6], [8, 5, 9, 3]]

    def test_construct(self):
        """File not found or test file not equal"""
        file_tri = pe18.construct_tri('docs/TestTriangle.txt')
        self.assertEqual(self.known_tri, file_tri)

    def test_file_not_found(self):
        """Not raising right exception"""
        self.assertRaises(IOError, pe18.construct_tri, 'filenotexistent.txt')

    def test_nav_left(self):
        """Left navigate not working"""
        test_point_l = pe18.nav_tri_left(self.known_tri, 0, 0)
        self.assertEqual(test_point_l, self.known_tri[1][0])

    def test_nav_right(self):
        """Right navigate not working"""
        test_point_r = pe18.nav_tri_right(self.known_tri, 0, 0)
        self.assertEqual(test_point_r, self.known_tri[1][1])

    def test_left_row_more_col(self):
        """Exception not raised in nav_left when row > col"""
        self.assertRaises(Exception, pe18.nav_tri_left, self.known_tri, 3, 4)

    def test_left_col_more_hei(self):
        """Exception not raised in nav left when col > len(tri)"""
        self.assertRaises(Exception, pe18.nav_tri_left, self.known_tri,
                            len(self.known_tri) + 1, 0)

    def test_right_row_more_col(self):
        """Exception not raised in nav_right when row > col"""
        self.assertRaises(Exception, pe18.nav_tri_right, self.known_tri, 3, 4)

    def test_right_col_more_hei(self):
        """Exception not raised in nav_right when col > len(tri)"""
        self.assertRaises(Exception, pe18.nav_tri_right, self.known_tri,
                            len(self.known_tri) + 1, 0)

    def test_max(self):
        """Maximum not equal, or maximum function error"""
        known_max = 23
        test_max = pe18.find_max(self.known_tri)
        self.assertEqual(known_max, test_max)
        
