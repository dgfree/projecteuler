# pe1_tests.py
import unittest
from nose.tools import *
from problem001 import problem001

class Test_Known(unittest.TestCase):
    known_solution = 23
    
    def test_solution(self):
        number = pe1.sum_of_3_and_5_mults(10)
        self.assertEqual(self.known_solution, number)
    
    def test_data_type(self):
        self.assertRaises(Exception, pe1.sum_of_3_and_5_mults, "Hello")
