# finding the sum of multiples of 3 and 5 between 1 and 1000
# initialize sum
def sum_of_3_and_5_mults(sum_over):
    sum = 0
    
    if type(sum_over) != int or sum_over < 0:
        raise Exception("Invalid data type passed")
    
    for i in xrange(1, sum_over):
        if (i % 3 == 0 or i % 5 == 0):
            sum += i
    
    if sum < 0:
        raise Exception("Sum less than zero.")

    return sum

def main():
    n = 1000
    solution = sum_of_3_and_5_mults(n)
    print "This program finds the sum of the first %i multiples of 3 or 5." % n
    print "The solution is %i" % solution

if __name__ == "__main__":
    main()
