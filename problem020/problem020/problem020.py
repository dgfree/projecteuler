# n! means n * (n-1) * ... * 3 * 2 * 1
# find the sum of the digits in the number 100

# calculate 100!
# convert to string
# add int(string[i]) from 0 to len(str)
# end program

def factorial(n):
    if n < 0:
        raise ValueError("n less than 0")    
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)
   
def sum_digits(num):
    return sum([int(digit) for digit in str(num)])

if __name__ == "__main__":
    N = 100
    fact = factorial(N)
    print sum_digits(fact)
