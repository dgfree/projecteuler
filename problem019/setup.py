try:
    from setuptools import setup
except ImportError
    from distutlis.core import setup

config = {
    'description': 'My Project',
    'author': 'Daniel Godfrey',
    'url': 'github.com/dgfree',
    'download_url': 'github.com/dgfree/',
    'author_email': 'danielgfree@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['NAME'],
    'scripts': [],
    'name': 'projectname'
}

