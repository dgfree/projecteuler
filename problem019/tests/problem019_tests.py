import unittest
from nose.tools import *
from pe19 import pe19

class TestSuite(unittest.TestCase):
    """Test suite"""    
    def test_leap_4(self):
        """Leap year divisible by 4 but not 100 is not true."""
        self.assertTrue(pe19.is_leap(1876))

    def test_leap_100(self):
        """Leap year divisible by 100 but not 400 is not false."""
        self.assertFalse(pe19.is_leap(1800))

    def test_leap_400(self):
        """Leap year divisible by 400 is not true."""
        self.assertTrue(pe19.is_leap(1600))

    def test_day_start_check(self):
        """check_start returning wrong value"""
        self.assertEquals(pe19.check_start(1901), 2)

    def test_day_start_exception(self):
        """check_start not raising the exception anymore"""
        self.assertRaises(Exception, pe19.check_start, 1850)

    def test_count_range(self):
        """count_first_sundays returning wrong value"""
        self.assertEquals(pe19.count_first_sundays(1951, 2000), 7)
