# pe19.py
# counts the number of sundays from 1 jan 1901 to 31 dec 2000
# that fell on the first of the month
# TODO: Better comments
#       Clean Up

import math


month_days = {1: 31, 2: 28, 3: 31, 4: 30,
    5: 31, 6: 30, 7: 31, 8: 31, 9: 30,
    10: 31, 11: 30, 12: 31}

leap_month_days = {1: 31, 2: 29, 3: 31, 4: 30,
    5: 31, 6: 30, 7: 31, 8: 31, 9: 30,
    10: 31, 11: 30, 12: 31}

def is_leap(year):
    """Checks if year is a leap year"""    
    if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0): 
        return True
    
    else:
        return False

def which_dict(year):
    """Chooses which dictionary to use."""
    if not is_leap(year):
        return month_days

    elif is_leap(year):
        return leap_month_days

    else:
        raise Exception    
    
def start_of_year(year):
    """Checks what day the year starts on"""
    start = 0    

    if year < 1900:
        raise Exception("Doesn't support years before 1900")
        
    if year == 1900:
        start = 1    

    elif is_leap(year - 1):
        start += start_of_year(year - 1) + 2
    
    else:
        start += start_of_year(year - 1) + 1
    
    return start % 7

def month_starts(year):
    start = []
    start.append(start_of_year(year))    
    dict_to_use = which_dict(year)

    for i in xrange(2, 13):
        start.append((dict_to_use[i] + start[i - 2]) % 7)

    return start

def count_first_sundays(start, end):
    """Counts first sundays"""
    count = 0
    for i in xrange(start, end):
        count += len([x for x in month_starts(i) if x == 0])
    return count

def main():
    START_YEAR = 1901
    END_YEAR = 2000    
    print count_first_sundays(START_YEAR, END_YEAR)

if __name__ == "__main__":
    main()
