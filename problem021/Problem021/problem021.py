import math

def sum_amicable(num1):
    """
    Determines whether the given numbers are amicable.
    Amicable - The sums of the divisors are equal to the other number.
    
    num1: first number input
    from num1 we construct the only possible amicable number, the sum of its
    divisors, 
    """

    num2 = sum_of_proper_divisors(num1)
    if num2 > num1 and sum_of_proper_divisors(num2) == num1:
        return num1 + num2
    else:
        return 0

def sum_of_divisors(n):
    """
    Sum of divisors using 
    http://mathschallenge.net/index.php?section=faq&ref=number/sum_of_divisors    
    """
    summ = 1
    p = 2
    while p*p <= n and n > 1:
        if n%p == 0:
            j = p*p
            n = n/p
            while n%p == 0:
                j = j*p
                n = n/p
            summ *= j-1
            summ /= p-1
        if p==2:
            p = 3
        else:
            p += 2
    if n>1:
        summ *= n+1
    return summ

def sum_of_proper_divisors(n):
    return sum_of_divisors(n) - n


def main():
    """
    Finds the sum of amicable pairs from 1 to MAX.
    """
    MAX = 10000
    summ = 0
    for num1 in xrange(1, MAX):
        summ += sum_amicable(num1)                
    # print(summ)
                
if __name__ == "__main__":
    import timeit
    print(timeit.repeat("main()", setup="from Problem021 import main",
                        number=30) / 30)


# old code that nobody likes
def sieve(limit):
    """
    Determines prime factors using the Sieve of Eratosthenes.
    Accepts an upper limit.
    
    Parameters:
    limit - The maximum number that we want to find prime factors up to
    """
    cross_limit = int(math.sqrt(limit))
    sieve = [False,] * limit

    for i in xrange(4, limit, 2):
        sieve[i] = True
    for i in xrange(3, cross_limit, 2):
        if not sieve[i]:
            for j in xrange(i*i, limit, 2*i):
                sieve[j] = True
    return sieve

def prime_factors(num):
    """
    Calculates the prime divisors of a number.
    Returns them as a list.
    
    Parameters
    num: number to find prime factors of.
    """
    divisors = []
    sieve_list = sieve(num)
    for i in xrange(2, len(sieve_list)):     
        if not sieve_list[i] and num % i == 0:
            # catch the powers of primes
            while num % i == 0 and num != 0:
                divisors.append(i)
                num /= i
    
    return divisors
    
def factor_generator(n):
    p = prime_factors(n)
    factors={}
    for p1 in p:
        try:
            factors[p1]+=1
        except KeyError:
            factors[p1]=1
    return factors

def proper_factors(n):
    """
    Returns a list of proper factors of n.
    
    Parameters:
    n - The number whose proper factors will be returned
    """
    factors = factor_generator(n)
    divisors=[]
    
    listexponents=[map(lambda x:k**x,range(0,factors[k]+1)) for k in factors.keys()]
    listfactors=cartesian_product(listexponents)
    
    for f in listfactors:
        divisors.append(reduce(lambda x, y: x*y, f, 1))
    
    divisors.sort()
    proper_factors = list(divisors)
    
    if proper_factors:
        del proper_factors[-1]
    
    return proper_factors

def cartesian_product(lists):
    """
    given a list of lists,
    returns all the possible combinations taking one element from each list
    The list does not have to be of equal length
    """
    return reduce(appendEs2Sequences,lists,[])

def appendEs2Sequences(sequences,es):
    result=[]
    if not sequences:
        for e in es:
            result.append([e])
    else:
        for e in es:
            result+=[seq+[e] for seq in sequences]
    return result
